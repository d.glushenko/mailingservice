import os

from celery import Celery
from celery.schedules import crontab
from .settings import TIME_ZONE

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'messageSenderService.settings')

app = Celery('messageSenderService')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()

#  настройки для отсылки статистики на email каждые сутки
app.conf.timezone = TIME_ZONE
app.conf.beat_schedule = {
    'send-stat-every-24-hours': {
        'task': 'messageSender.tasks.send_stat_email',
        'schedule': crontab(minute=0, hour=0)
    }
}
