from rest_framework import serializers

from .models import Mailing, Message, Client, StatMail


class StatMailSerializer(serializers.ModelSerializer):

    class Meta:
        model = StatMail
        fields = ('email',)


class MessageSerializer(serializers.ModelSerializer):

    class Meta:
        model = Message
        fields = ('pk', 'dt', 'status', 'mailing', 'client')


class MailingSerializer(serializers.ModelSerializer):
    count_all_messages = serializers.SerializerMethodField(read_only=True)
    count_sended_messages = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Mailing
        fields = (
            'pk',
            'mailing_start_dt',
            'text',
            'filter_property',
            'mailing_end_dt',
            'client_local_dt_start',
            'client_local_dt_end',
            'count_all_messages',
            'count_sended_messages'
        )

    def get_count_all_messages(self, obj):
        return obj.messages.count()

    def get_count_sended_messages(self, obj):
        return obj.messages.filter(status='sended').count()


class SingleMailingSerializer(serializers.ModelSerializer):
    messages = MessageSerializer

    class Meta:
        model = Mailing
        fields = ('pk', 'dt_start', 'text', 'filter_property', 'dt_end', 'messages')


class ClientSerializer(serializers.ModelSerializer):

    class Meta:
        model = Client
        fields = ('pk', 'phone_number', 'def_code', 'tag', 'timezone')
