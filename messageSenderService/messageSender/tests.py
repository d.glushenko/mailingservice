from rest_framework import status
from rest_framework.test import APIRequestFactory, APITestCase

from .models import Client
from .views import ClientViewSet

# Create your tests here.


class ClientAPITestCase(APITestCase):

    def setUp(self) -> None:
        self.test_client = Client.objects.create(
            phone_number='79184421525',
            def_code='918',
            tag='',
            timezone='Europe/Moscow'
        )

        self.data = {
            'phone_number': '79104421525',
            'def_code': '910',
            'tag': 'Da',
            'timezone': 'Europe/Moscow'
        }

    def test_create_client(self):
        request = APIRequestFactory().post("", data=self.data)
        client_detail = ClientViewSet.as_view({'post': 'create'})
        response = client_detail(request, self.data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data.get('def_code'), '910')

    def test_client_list(self):
        request = APIRequestFactory().get("")
        clients = ClientViewSet.as_view({'get': 'list'})
        response = clients(request, pk=self.test_client.pk)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_fail_client_details(self):
        request = APIRequestFactory().get("")
        client_detail = ClientViewSet.as_view({'get': 'retrieve'})
        response = client_detail(request, pk=50)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_client_details(self):
        request = APIRequestFactory().get("")
        client_detail = ClientViewSet.as_view({'get': 'retrieve'})
        response = client_detail(request, pk=self.test_client.pk)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get('phone_number'), '79184421525')

    def test_update_client(self):
        request = APIRequestFactory().put("", data=self.data)
        client_detail = ClientViewSet.as_view({'put': 'update'})
        response = client_detail(request, self.data, pk=self.test_client.pk)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get('def_code'), '910')
