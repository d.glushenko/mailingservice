from django.urls import path
from rest_framework.routers import DefaultRouter

from .views import ClientViewSet, MailingViewSet, StatMailViewSet


app_name = 'messageSender'
router = DefaultRouter()
router.register(r'client', ClientViewSet, basename='client')
router.register(r'mailing', MailingViewSet, basename='mailing')
router.register(r'statmail', StatMailViewSet, basename='statmail')
urlpatterns = router.urls
