from django.db import models
import pytz

from .validators import filters_validator, dt_validator


# Create your models here.


class Mailing(models.Model):
    mailing_start_dt = models.DateTimeField(validators=[dt_validator])
    text = models.TextField()
    filter_property = models.JSONField(validators=[filters_validator])
    mailing_end_dt = models.DateTimeField(validators=[dt_validator])
    client_local_dt_start = models.DateTimeField()
    client_local_dt_end = models.DateTimeField()


class Client(models.Model):
    TIMEZONE_CHOICES = zip(pytz.all_timezones, pytz.all_timezones)

    phone_number = models.CharField(max_length=11)
    def_code = models.CharField(max_length=3)
    tag = models.CharField(max_length=255)
    timezone = models.CharField(max_length=255, default='UTC', choices=TIMEZONE_CHOICES)


class Message(models.Model):
    statuses = (
        ('not sended', 'not sended'),
        ('sended', 'sended'),
    )

    dt = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=255, choices=statuses, default='not sended')
    mailing = models.ForeignKey(Mailing, on_delete=models.CASCADE, related_name='messages')
    client = models.ForeignKey(Client, on_delete=models.CASCADE)


class StatMail(models.Model):
    email = models.EmailField()
