import json
from datetime import datetime, timedelta
from typing import Dict

from django.conf import settings
from django.core.mail import send_mail
from django.utils.dateparse import parse_datetime
from pytz import timezone
import requests

from messageSenderService.celery import app
from .models import Client, Message, Mailing, StatMail


def create_and_send_message(client, data):
    msg = Message.objects.create(
        mailing=Mailing.objects.get(pk=data['pk']),
        client=client
    )
    msg.save()
    request_body = {
        "id": msg.pk,
        "phone": client.phone_number,
        "text": data['text']
    }
    if parse_datetime(data['client_local_dt_start']) <= datetime.now(tz=client.timezone)\
            <= parse_datetime(data['client_local_dt_end']):
        res = requests.post(
            headers={'Authorization': f'Bearer {settings.TOKEN}'},
            url=f'{settings.MAILING_SERVICE_URL}/{str(msg.pk)}',
            data=json.dumps(request_body)
        )
        if res.status_code == 200:
            msg.status = 'sended'
            msg.save()
        else:
            # тут логгирование прикрутить надо
            raise requests.RequestException
    else:
        # тут логгирование прикрутить надо, что не прошла timezone клиента
        msg.save()


@app.task(autoretry_for=(requests.RequestException,), default_retry_delay=60)
def start_mailing(data: Dict):
    try:
        if data['filter_property']['def_code'] == '':
            data['filter_property'].pop('def_code')
    except KeyError:
        pass
    try:
        if data['filter_property']['tag'] == '':
            data['filter_property'].pop('tag')
    except KeyError:
        pass
    if data['filter_property']:
        for client in Client.objects.filter(**data['filter_property']):
            create_and_send_message(client, data)
    else:
        for client in Client.objects.all():
            create_and_send_message(client, data)


@app.task
def send_stat_email():
    mailings = Mailing.objects.filter(
        dt_end__gt=datetime.now(tz=timezone(settings.TIME_ZONE)) - timedelta(hours=24),
        dt_end__lt=datetime.now(tz=timezone(settings.TIME_ZONE))
    )
    mail_text = ''
    for mailing in mailings:
        mail_text += f'''Рассылка №{mailing.pk}, время старта - {mailing.dt_start}, время конца - {mailing.dt_end},\n 
        фильтры: код оператора - {mailing.filter_property.get('def_code')}, тег - {mailing.filter_property.get('tag')}\n
        отправлено: {mailing.messages.filter(status='sended').count()}, не отправлено 
        {mailing.messages.filter(status='not sended').count()}\n'''
    send_mail(
        'Статистика по рассылкам за сутки',
        mail_text,
        settings.EMAIL_HOST_USER,
        StatMail.objects.values_list('email', flat=True)
    )
