from datetime import datetime
from typing import Dict

from django.conf import settings
from pytz import timezone

from django.core.validators import ValidationError


def filters_validator(filters: Dict):
    # валидатор на проверку, соответствует ли словарь условиям
    if 'def_code' not in filters or 'tag' not in filters:
        raise ValidationError('Неправильно заполнено поле "filter_property"')
    for key in filters.keys():
        if key != 'def_code' and key != 'tag':
            raise ValidationError('Добавлены лишние фильтры')


def dt_validator(dt: datetime):
    if dt < datetime.now(tz=timezone(settings.TIME_ZONE)):
        raise ValidationError('Дата и время не должны быть меньше текущих')
