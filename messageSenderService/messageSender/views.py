from django.shortcuts import get_object_or_404
from django.utils.dateparse import parse_datetime

from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi

from rest_framework import status
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from .models import Client, Mailing, StatMail, Message
from .serializer import (
    ClientSerializer, MailingSerializer, SingleMailingSerializer, StatMailSerializer
)

from django.conf import settings
import requests
from .tasks import start_mailing

# Create your views here.


class StatMailViewSet(ModelViewSet):
    permission_classes = (AllowAny,)
    serializer_class = StatMailSerializer
    queryset = StatMail.objects.all()
    pagination_class = LimitOffsetPagination


class ClientViewSet(ModelViewSet):
    permission_classes = (AllowAny,)
    serializer_class = ClientSerializer
    queryset = Client.objects.all()
    pagination_class = LimitOffsetPagination


class MailingViewSet(ModelViewSet):
    SWAGGER_AUTO_SCHEMA = dict(request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                'mailing_start_dt': openapi.Schema(type=openapi.TYPE_STRING),
                'text': openapi.Schema(type=openapi.TYPE_STRING),
                'client_local_dt_start': openapi.Schema(type=openapi.TYPE_STRING),
                'client_local_dt_end': openapi.Schema(type=openapi.TYPE_STRING),
                'filter_property': openapi.Schema(
                    type=openapi.TYPE_OBJECT,
                    properties={
                        'def_code': openapi.Schema(type=openapi.TYPE_STRING),
                        'tag': openapi.Schema(type=openapi.TYPE_STRING)
                    }
                ),
                'mailing_end_dt': openapi.Schema(type=openapi.TYPE_STRING)
            }
        ),
        responses={
            status.HTTP_201_CREATED: openapi.Schema(
                type=openapi.TYPE_OBJECT,
                properties={
                    'pk': openapi.Schema(type=openapi.TYPE_INTEGER, read_only=True),
                    'mailing_start_dt': openapi.Schema(type=openapi.TYPE_STRING),
                    'text': openapi.Schema(type=openapi.TYPE_STRING),
                    'client_local_dt_start': openapi.Schema(type=openapi.TYPE_STRING),
                    'client_local_dt_end': openapi.Schema(type=openapi.TYPE_STRING),
                    'filter_property': openapi.Schema(
                        type=openapi.TYPE_OBJECT,
                        properties={
                            'def_code': openapi.Schema(type=openapi.TYPE_STRING),
                            'tag': openapi.Schema(type=openapi.TYPE_STRING)
                        }
                    ),
                    'mailing_end_dt': openapi.Schema(type=openapi.TYPE_STRING),
                    'count_all_messages': openapi.Schema(type=openapi.TYPE_STRING),
                    'count_sended_messages': openapi.Schema(type=openapi.TYPE_STRING)
                }
            )
        }
    )

    permission_classes = (AllowAny,)
    serializer_class = MailingSerializer
    queryset = Mailing.objects.all()
    pagination_class = LimitOffsetPagination

    @swagger_auto_schema(**SWAGGER_AUTO_SCHEMA)
    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        start_time = parse_datetime(serializer.data['mailing_start_dt'])
        end_time = parse_datetime(serializer.data['mailing_end_dt'])
        start_mailing.apply_async((serializer.data,), eta=start_time, expires=end_time)
        return Response(data=serializer.data, status=status.HTTP_201_CREATED)

    @swagger_auto_schema(**SWAGGER_AUTO_SCHEMA)
    def update(self, request, *args, **kwargs):
        return super.update(self, request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        pk = self.kwargs['pk']
        queryset = Mailing.objects.all()
        user = get_object_or_404(queryset, pk=pk)
        serializer = SingleMailingSerializer(user)
        return Response(data=serializer.data)
